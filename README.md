# My Weather Project

This project is a weather project with current, hourly, and seven day forecasts available. The weather defaults to your location using your phone's built in GPS. You can also change the weather location using Google's Geolocation API.

## API Usage

	- Used Google Geolocation API for location autocomplete.
	- Used the Weather Channel API for weather forecasts.
	
## Third Party SDKs Used

	- Used several third party SDKs that I like to use in my own projects.
	- Retrofit for making REST calls.
	- OKHTTP as an HTTP client.
	- MaterialProgressBar for beautiful loading.
	- GreenRobot EventBus for sending broadcasts.