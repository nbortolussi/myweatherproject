package com.myweatherproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements PlaceSelectionListener, ConfirmInterface {

    private static final int REQUEST_LOCATION_PERMISSION = 9000;
    private static final int REQUEST_GPS = 9001;
    private static final int REQUEST_LOCATION_PERMISSION_ALREADY_DENIED = 9002;

    private GetWeather getWeather;
    private MaterialProgressBar loadingprogress;
    private LinearLayout weatherCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupActivity();
    }

    public void setupActivity() {
        loadingprogress = (MaterialProgressBar) findViewById(R.id.loadingprogress);
        weatherCard = (LinearLayout) findViewById(R.id.weatherCard);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        if (autocompleteFragment != null)
            autocompleteFragment.setOnPlaceSelectedListener(this);

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (getWeather != null)
            getWeather.cancel(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getLocation(true);
    }

    @Override
    public void onPlaceSelected(Place place) {
        hideSearch();
        startLoading();
        Location location = new Location(place);
        getWeather(location);
    }

    @Override
    public void onError(Status status) {
        Log.e(getClass().getName(), "Failed getting location... " + status.toString());
    }

    public void getLocation(boolean acceptCached) {
        // make sure we have the proper permissions to access location in android 6
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
        } else {

            if (acceptCached) {
                // grab our most recent location and use that to populate our weather first, if available.
                try {
                    LocationManager locationManager = (LocationManager) getSystemService(Activity.LOCATION_SERVICE);
                    android.location.Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                    if (location != null) {
                        getWeather(new Location(getString(R.string.my_location), location));
                        return;
                    }
                } catch (SecurityException e) {
                    Log.e(getClass().getName(), "Get updated location failed. Permission error!", e);
                }
            }

            // if we don't have a most recent location saved, let's start our location grabber service
            if (runIsGpsEnabledCheck()) {
                Intent intent = new Intent(this, LocationService.class);
                startService(intent);
            }
        }
    }


    public void getWeather(Location location) {
        getWeather = new GetWeather();
        getWeather.execute(location);
    }

    public class GetWeather extends AsyncTask<Location, Void, Boolean> {

        private Weather weather;

        @Override
        protected Boolean doInBackground(Location... locationPassed) {

            Location location = locationPassed[0];
            weather = new Weather(location);

            try {
                Response<Weather.CurrentAndHourly> hourlyResponse = RestClient.getWeatherRestClient().getHourly(location.getLatitude(), location.getLongitude()).execute();
                weather.setCurrentAndHourly(hourlyResponse.body());

                Response<Weather.SevenDay> sevenDayResponse = RestClient.getWeatherRestClient().getSevenDay(location.getLatitude(), location.getLongitude()).execute();
                weather.setSevenDay(sevenDayResponse.body());
            } catch (IOException e) {
                Log.e(getClass().getName(), "Oops, our call to get weather failed.", e);
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success)
                setupWeather(weather);
            else
                stopLoading();
        }
    }

    // runs a check to see if the user's GPS is enabled.
    public boolean runIsGpsEnabledCheck() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS)) {
            LocationManager manager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showConfirmDialog(REQUEST_GPS, null, getString(R.string.gps_disable), getString(R.string.enable));
                return false;
            }
        }

        return true;
    }

    // shows a standard confirmation dialog
    public void showConfirmDialog(int id, String title, String body, String affirmative) {
        ConfirmFragment confirmFragment = ConfirmFragment.newInstance(id, title, body, affirmative);
        confirmFragment.show(getSupportFragmentManager(), ConfirmFragment.class.getName());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void locationReceived(Location location) {
        getWeather(location);
    }

    @Override
    public void confirmationReceived(int id, boolean response) {
        if (response && id == REQUEST_GPS) {
            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            if (intent.resolveActivityInfo(getPackageManager(), 0) != null)
                startActivity(intent);
            else
                Toast.makeText(this, getString(R.string.gps_settings_error), Toast.LENGTH_SHORT).show();
        } else if (response && id == REQUEST_LOCATION_PERMISSION_ALREADY_DENIED) {
            loadingprogress.setVisibility(View.GONE);
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            if (intent.resolveActivityInfo(getPackageManager(), 0) != null) {
                Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            } else
                Toast.makeText(this, getString(R.string.settings_error), Toast.LENGTH_SHORT).show();
        } else if (id == REQUEST_GPS) {
            loadingprogress.setVisibility(View.GONE);
            Toast.makeText(this, getString(R.string.enable_gps), Toast.LENGTH_SHORT).show();
        } else if (id == REQUEST_LOCATION_PERMISSION_ALREADY_DENIED) {
            loadingprogress.setVisibility(View.GONE);
            Toast.makeText(this, getString(R.string.enable_permissions), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    getLocation(true);
                else if (shouldShowRequestPermissionRationale(permissions[0])) {
                    loadingprogress.setVisibility(View.GONE);
                    Toast.makeText(this, getString(R.string.enable_permissions), Toast.LENGTH_SHORT).show();
                } else {
                    showConfirmDialog(REQUEST_LOCATION_PERMISSION_ALREADY_DENIED, null, getString(R.string.denied_permission), getString(R.string.enable));
                }

                break;
            }
        }
    }

    public void startLoading() {
        loadingprogress.setVisibility(View.VISIBLE);
        weatherCard.setVisibility(View.GONE);
    }

    public void stopLoading() {
        loadingprogress.setVisibility(View.GONE);
        weatherCard.setVisibility(View.VISIBLE);
    }

    public void hourlyClicked(View v) {
        findViewById(R.id.hourly).setBackgroundResource(R.drawable.background_weather_selected);
        findViewById(R.id.sevenDay).setBackgroundResource(R.drawable.selector_weather_toggle);
        findViewById(R.id.hourRV).setVisibility(View.VISIBLE);
        findViewById(R.id.dayRV).setVisibility(View.GONE);
    }

    public void sevenDayClicked(View v) {
        findViewById(R.id.sevenDay).setBackgroundResource(R.drawable.background_weather_selected);
        findViewById(R.id.hourly).setBackgroundResource(R.drawable.selector_weather_toggle);
        findViewById(R.id.dayRV).setVisibility(View.VISIBLE);
        findViewById(R.id.hourRV).setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_search:
                if (findViewById(R.id.searchCV).getVisibility() == View.GONE)
                    findViewById(R.id.searchCV).setVisibility(View.VISIBLE);
                else
                    hideSearch();
                break;
            case R.id.menu_my_location:
                startLoading();
                getLocation(false);
                break;
        }
        return true;
    }

    public void hideSearch() {
        findViewById(R.id.searchCV).setVisibility(View.GONE);
    }

    public void setupWeather(Weather weather) {
        setupHourlyAndSevenDayForecast(weather);
        setupCurrent(weather);
        stopLoading();
    }

    public void setupHourlyAndSevenDayForecast(Weather weather) {
        if (weather.hasValidHourlyForecast()) {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.hourRV);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            WeatherHourAdapter weatherHourAdapter = new WeatherHourAdapter(getApplicationContext(), weather.currentAndHourly.fcsthourly24.data.forecasts);
            recyclerView.setAdapter(weatherHourAdapter);
        }
        if (weather.hasValidSevenDayForecast()) {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.dayRV);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerView.setLayoutManager(linearLayoutManager);
            WeatherDayAdapter weatherDayAdapter = new WeatherDayAdapter(getApplicationContext(), weather.sevenDay.forecasts);
            recyclerView.setAdapter(weatherDayAdapter);
        }
    }

    public void setupCurrent(Weather weather) {
        if (weather.hasValidCurrentData()) {
            String prefix = "icon_weather_";
            int id = getResources().getIdentifier(prefix + Integer.toString(weather.getIconCode()), "drawable", getPackageName());
            if (id == 0)
                id = R.drawable.icon_weather_unknown;
            ((ImageView) findViewById(R.id.weatherIcon)).setImageResource(id);
            ((TextView) findViewById(R.id.tempHigh)).setText(weather.getHighTemp());
            ((TextView) findViewById(R.id.tempLow)).setText(weather.getLowTemp());
            ((TextView) findViewById(R.id.temp)).setText(weather.getTemp());
            ((TextView) findViewById(R.id.dateTV)).setText(weather.getDate());
            ((TextView) findViewById(R.id.weatherDescription)).setText(weather.getDescription());
            ((TextView) findViewById(R.id.locationTV)).setText(weather.getTitle());
            ((TextView) findViewById(R.id.sunrisesunsetTV)).setText(getString(R.string.sunrise) + ": " + weather.getSunrise() + " / " + getString(R.string.sunset) + ": " + weather.getSunset());
        }
    }
}
