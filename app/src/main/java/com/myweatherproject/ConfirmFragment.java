package com.myweatherproject;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;


public class ConfirmFragment extends DialogFragment {

    private ConfirmInterface confirmInterface;
    private int id;
    private String title;
    private String message;
    private String affirmative;

    public static ConfirmFragment newInstance(int id, String title, String body, String affirmative) {
        ConfirmFragment frag = new ConfirmFragment();
        Bundle args = new Bundle();
        args.putInt("id", id);
        args.putString("title", title);
        args.putString("body", body);
        args.putString("affirmative", affirmative);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (confirmInterface != null)
            confirmInterface.confirmationReceived(id, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof ConfirmInterface)
            confirmInterface = (ConfirmInterface) activity;
        else
            throw new IllegalArgumentException("Our activity doesn't implement our confirm interface!");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        id = getArguments().getInt("id");
        title = getArguments().getString("title");
        message = getArguments().getString("body");
        affirmative = getArguments().getString("affirmative");

        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_fragment_confirm);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        TextView bodyTV = (TextView) dialog.findViewById(R.id.body);
        bodyTV.setText(message);

        TextView titleTV = (TextView) dialog.findViewById(R.id.title);
        if (StringUtils.isEmpty(title))
            titleTV.setVisibility(View.GONE);
        else {
            titleTV.setVisibility(View.VISIBLE);
            titleTV.setText(title);
        }

        Button yes = (Button) dialog.findViewById(R.id.affirmative);
        yes.setText(affirmative);
        Button no = (Button) dialog.findViewById(R.id.negative);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                confirmInterface.confirmationReceived(id, true);
                confirmInterface = null;
                dismiss();
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                confirmInterface.confirmationReceived(id, false);
                confirmInterface = null;
                dismiss();
            }
        });

        return dialog;
    }
}