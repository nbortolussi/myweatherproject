package com.myweatherproject;

public interface ConfirmInterface {

	void confirmationReceived(int id, boolean response);
}
