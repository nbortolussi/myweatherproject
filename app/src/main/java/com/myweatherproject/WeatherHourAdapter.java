package com.myweatherproject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class WeatherHourAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Weather.CurrentAndHourly.TwentyFour.Data.Hour> data;
    private String prefix;

    public WeatherHourAdapter(Context context, ArrayList<Weather.CurrentAndHourly.TwentyFour.Data.Hour> data) {
        super();
        this.context = context;
        this.data = data;
        this.prefix = "icon_weather_";
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        HourViewHolder holder = (HourViewHolder) viewHolder;
        Weather.CurrentAndHourly.TwentyFour.Data.Hour hour= data.get(position);
        int id = context.getResources().getIdentifier(prefix + Integer.toString(hour.icon_code), "drawable", context.getPackageName());
        if (id == 0)
            id = R.drawable.icon_weather_unknown;
        holder.icon.setImageResource(id);
        holder.temp.setText(Integer.toString(hour.temp) + (char) 0x00B0);
        holder.time.setText(hour.getHour());
        holder.wind.setText(Integer.toString(hour.wspd) + " mph " + hour.wdir_cardinal);
        holder.visibility.setText(Integer.toString(hour.vis) + " m");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_weather_hour, viewGroup, false);
        return new HourViewHolder(itemView);
    }

    public static class HourViewHolder extends RecyclerView.ViewHolder {
        protected ImageView icon;
        protected TextView time;
        protected TextView temp;
        protected TextView wind;
        protected TextView visibility;

        public HourViewHolder(View v) {
            super(v);

            time = (TextView) v.findViewById(R.id.time);
            temp = (TextView) v.findViewById(R.id.temp);
            wind = (TextView) v.findViewById(R.id.wind);
            visibility = (TextView) v.findViewById(R.id.visibility);
            icon = (ImageView) v.findViewById(R.id.icon);
        }
    }
}