package com.myweatherproject;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class WeatherDayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<Weather.SevenDay.Day> data;
    private String prefix;

    public WeatherDayAdapter(Context context, ArrayList<Weather.SevenDay.Day> data) {
        super();
        this.context = context;
        this.data = data;
        this.prefix = "icon_weather_";
    }

    @Override
    public int getItemCount() {
        return data.size() - 1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        DayViewHolder holder = (DayViewHolder) viewHolder;
        Weather.SevenDay.Day day = data.get(position + 1);
        int id = context.getResources().getIdentifier(prefix + Integer.toString(day.day.icon_code), "drawable", context.getPackageName());
        if (id == 0)
            id = R.drawable.icon_weather_unknown;
        holder.icon.setImageResource(id);
        holder.tempLo.setText(Integer.toString(day.day.wc) + (char) 0x00B0);
        holder.tempHi.setText(Integer.toString(day.day.hi) + (char) 0x00B0);
        holder.day.setText(day.getDay());
        holder.wind.setText(Integer.toString(day.day.wspd) + " mph " + day.day.wdir_cardinal);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_weather_day, viewGroup, false);
        return new DayViewHolder(itemView);
    }

    public static class DayViewHolder extends RecyclerView.ViewHolder {
        protected ImageView icon;
        protected TextView day;
        protected TextView tempHi;
        protected TextView wind;
        protected TextView tempLo;

        public DayViewHolder(View v) {
            super(v);

            day = (TextView) v.findViewById(R.id.day);
            tempHi = (TextView) v.findViewById(R.id.tempHi);
            wind = (TextView) v.findViewById(R.id.wind);
            tempLo = (TextView) v.findViewById(R.id.tempLo);
            icon = (ImageView) v.findViewById(R.id.icon);
        }
    }
}