package com.myweatherproject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by nbortolussi on 5/20/16.
 */
public class RestClient {

    private static WeatherInterface weatherInterface;

    public static WeatherInterface getWeatherRestClient() {
        if (weatherInterface == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.weather.com/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            weatherInterface = retrofit.create(WeatherInterface.class);
        }

        return weatherInterface;
    }

    public interface WeatherInterface {

        String apiKey = "0f84404bf111bd92716f99e2cff783a3";

        @GET("v1/geocode/{lat}/{lng}/forecast/daily/7day.json?apiKey=" + apiKey)
        Call<Weather.SevenDay> getSevenDay(@Path("lat") double lat, @Path("lng") double lng);

        @GET("v2/geocode/{lat}/{lng}/aggregate.json?products=conditions,fcsthourly24&apiKey=" + apiKey + "&units=e")
        Call<Weather.CurrentAndHourly> getHourly(@Path("lat") double lat, @Path("lng") double lng);
    }
}
