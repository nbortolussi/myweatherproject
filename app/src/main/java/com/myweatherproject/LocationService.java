package com.myweatherproject;

import android.app.Service;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.Semaphore;

public class LocationService extends Service implements LocationListener {

    private LocationManager locationManager;
    private final Semaphore locationReceived = new Semaphore(1);

    public void setupLocationUpdates() {
        try {
            locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        } catch (SecurityException e) {
            Log.e(getClass().getName(), "Get updated location failed. Permission error!", e);
            stopSelf();
        }
    }

    public void removeLocationUpdates() {
        try {
            locationManager.removeUpdates(this);
        } catch (SecurityException e) {
            Log.e(getClass().getName(), "Get updated location failed. Permission error!", e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeLocationUpdates();
        locationReceived.release();
    }

    @Override
    public void onCreate() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        setupLocationUpdates();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startid) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void sendLocationReceivedBroadcast(Location location) {
        if (locationReceived.tryAcquire()) {
            EventBus.getDefault().post(location);
            stopSelf();
        }
    }

    @Override
    public synchronized void onLocationChanged(android.location.Location location) {
        Log.i(getClass().getName(), "My location received! Location provider is: " + location.getProvider() + ", accuracy is: " + location.getAccuracy() + "m, latitude coordinate is: " + location.getLatitude() + ", and longitude coordinate is: " + location.getLongitude());
        sendLocationReceivedBroadcast(new Location(getString(R.string.my_location), location));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}