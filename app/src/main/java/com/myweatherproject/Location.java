package com.myweatherproject;

import com.google.android.gms.location.places.Place;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by nbortolussi on 5/20/16.
 */
public class Location {
    private android.location.Location location;
    private Place place;
    private String title;

    public Location(android.location.Location location, Place place) {
        this.location = location;
        this.place = place;
    }

    public Location(android.location.Location location) {
        this.location = location;
    }

    public Location(String title, android.location.Location location) {
        this.title = title;
        this.location = location;
    }

    public Location(Place place) {
        this.place = place;
    }

    public String getTitle() {
        if (StringUtils.isNotEmpty(title))
            return title;

        return place.getName().toString();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public android.location.Location getLocation() {
        return location;
    }

    public void setLocation(android.location.Location location) {
        this.location = location;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public double getLatitude() {
        if (location != null)
            return location.getLatitude();

        return place.getLatLng().latitude;
    }

    public double getLongitude() {
        if (location != null)
            return location.getLongitude();

        return place.getLatLng().longitude;
    }
}
