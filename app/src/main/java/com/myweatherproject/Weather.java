package com.myweatherproject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by nbortolussi on 5/20/16.
 */
public class Weather {

    public SevenDay sevenDay;
    public CurrentAndHourly currentAndHourly;
    public Location location;

    public Weather(Location location) {
        this.location = location;
    }

    public void setSevenDay(SevenDay sevenDay) {
        this.sevenDay = sevenDay;
    }

    public boolean hasValidHourlyForecast() {
        return currentAndHourly != null && currentAndHourly.fcsthourly24 != null && currentAndHourly.fcsthourly24.data != null && currentAndHourly.fcsthourly24.data.forecasts != null && currentAndHourly.fcsthourly24.data.forecasts.size() > 0;
    }

    public boolean hasValidSevenDayForecast() {
        return sevenDay != null && sevenDay.forecasts != null && sevenDay.forecasts.size() > 0;
    }

    public boolean hasValidCurrentData() {
        return currentAndHourly.conditions != null && currentAndHourly.conditions.data != null && currentAndHourly.conditions.data.observation != null;
    }

    public String getSunrise() {
        return convertLocalTimeStampToFormattedTimeString(currentAndHourly.conditions.data.observation.sunrise);
    }

    public String getSunset() {
        return convertLocalTimeStampToFormattedTimeString(currentAndHourly.conditions.data.observation.sunset);
    }

    public void setCurrentAndHourly(CurrentAndHourly currentAndHourly) {
        this.currentAndHourly = currentAndHourly;
    }

    public String getTitle() {
        return location.getTitle();
    }

    public String getDescription() {
        return currentAndHourly.conditions.data.observation.phrase_32char;
    }

    public int getIconCode() {
        return currentAndHourly.conditions.data.observation.icon_code;
    }

    public String getTemp() {
        return Integer.toString(currentAndHourly.conditions.data.observation.imperial.temp) + getTempUnit();
    }

    public String getHighTemp() {
        return Integer.toString(currentAndHourly.conditions.data.observation.imperial.hi) + getTempUnit();
    }

    public String getLowTemp() {
        return Integer.toString(currentAndHourly.conditions.data.observation.imperial.wc) + getTempUnit();
    }

    public char getTempUnit() {
        return (char) 0x00B0;
    }

    public String getDate() {
        return convertLocalTimeStampToFormattedDateString(currentAndHourly.conditions.data.observation.obs_time_local);
    }

    public static class SevenDay {
        public ArrayList<Day> forecasts;

        public static class Day {
            public String fcst_valid_local;
            public TimeOfDay day;
            public TimeOfDay night;

            public String getDay() {
                return getDayFromTimeStamp(fcst_valid_local);
            }

            public static String getDayFromTimeStamp(String timestamp) {
                try {
                    timestamp = timestamp.replaceAll("[^0-9]", "");
                    timestamp = timestamp.substring(0, 14);
                    SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                    format.setTimeZone(TimeZone.getDefault());
                    Date date = format.parse(timestamp);
                    format = new SimpleDateFormat("EEEE");
                    return format.format(date);

                } catch (Exception e) {
                    e.printStackTrace();
                    return timestamp;
                }
            }
        }

        public static class TimeOfDay {
            public int icon_code;
            public int wspd;
            public int wc;
            public int hi;
            public String wdir_cardinal;
        }
    }

    public static class CurrentAndHourly {
        public Conditions conditions;
        public TwentyFour fcsthourly24;

        public static class Conditions {

            public Data data;

            public static class Data {

                public Observation observation;

                public static class Observation {
                    public String sunrise;
                    public String sunset;
                    public String phrase_32char;
                    public int icon_code;
                    public Imperial imperial;
                    public String obs_time_local;

                    public static class Imperial {
                        public int temp;
                        public int hi;
                        public int wc;
                    }
                }
            }
        }

        public static class TwentyFour {
            public Data data;

            public static class Data {
                public ArrayList<Hour> forecasts;

                public static class Hour {
                    public int temp;
                    public int icon_code;
                    public int wspd;
                    public int vis;
                    public String wdir_cardinal;
                    public String fcst_valid_local;

                    public String getHour() {
                        return convertLocalTimeStampToFormattedTimeString(fcst_valid_local);
                    }

                    public static String getHourFromTimeStamp(String ts) {
                        try {
                            String timestamp = ts;
                            timestamp = timestamp.replaceAll("[^0-9]", "");
                            timestamp = timestamp.substring(0, 14);
                            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                            format.setTimeZone(TimeZone.getDefault());
                            Date date = format.parse(timestamp);
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            return Integer.toString(calendar.get(Calendar.HOUR_OF_DAY)) + ":00";
                        } catch (ParseException e) {
                            e.printStackTrace();
                            return "0";
                        }
                    }
                }
            }
        }
    }

    public static String convertLocalTimeStampToFormattedTimeString(String timestamp) {
        try {
            timestamp = timestamp.replaceAll("[^0-9]", "");
            timestamp = timestamp.substring(0, 14);
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            format.setTimeZone(TimeZone.getDefault());
            Date date = format.parse(timestamp);
            format = new SimpleDateFormat("hh:mm a");
            return format.format(date);

        } catch (Exception e) {
            e.printStackTrace();
            return timestamp;
        }
    }


    public static String convertLocalTimeStampToFormattedDateString(String timestamp) {
        try {
            timestamp = timestamp.replaceAll("[^0-9]", "");
            timestamp = timestamp.substring(0, 14);
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            format.setTimeZone(TimeZone.getDefault());
            Date date = format.parse(timestamp);
            format = new SimpleDateFormat("MMM dd yyyy HH:mm");
            return format.format(date);

        } catch (Exception e) {
            e.printStackTrace();
            return timestamp;
        }
    }
}
